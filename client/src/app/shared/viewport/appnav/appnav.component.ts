import { Component } from '@angular/core'
import { Routes, ROUTER_DIRECTIVES } from '@angular/router'

@Component({
  moduleId : module.id,
  selector : 'app-nav',
  templateUrl : './appnav.component.html',
  directives: [ROUTER_DIRECTIVES]
})

export class AppNav{
  
}
