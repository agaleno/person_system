import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name : 'sortBy'
})


export class SortByPipe{
  transform(value : any, column : string, order: string){
    // console.log('args = ' + column + ' order = ' + order);
    let direction = order == 'des' ? -1 : 1;
    if(!column || !value){
      return value;
    }else{
      return value.sort((a:any,b:any) => {
        if(column=='firstname'){
          return a.firstname.localeCompare(b.firstname) * direction ;
        }
        if(column=='middlename'){
          return a.lastname.localeCompare(b.lastname) * direction ;
        }
        if(column=='lastname'){
          return a.lastname.localeCompare(b.lastname) * direction ;
        }
        if(column=='city'){
          return a.city.localeCompare(b.city) * direction ;
        }
        if(column=='country'){
          return a.country.localeCompare(b.country) * direction ;
        }
        if(column=='creationdate'){
          // console.log('sortby creation date = ' + (Date.parse(a.creationdate) - Date.parse(b.creationdate) * direction))
          return Date.parse(a.creationdate) - Date.parse(b.creationdate) * direction;
        }
      })
    }

  }
}
