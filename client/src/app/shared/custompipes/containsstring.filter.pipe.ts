import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name : 'containsString'
})

export class ContainsStringPipe implements PipeTransform{
  transform(arraySource : any, searchString : string){
    console.log('searchString = ' + searchString);

//    arraySource.forEach((p) => console.log(p.firstname+p.middlename+p.lastname));

    if(searchString){
      let keyword = searchString.toLocaleLowerCase();
      if(arraySource){
        return arraySource.filter( person => (person.firstname+person.middlename+person.lastname).toLocaleLowerCase().indexOf(keyword) != -1 )
      }else{
        return [{id:0, firstname : 'no keyword entered'}];
      }
    }else{
      return arraySource;
    }

  }
}
