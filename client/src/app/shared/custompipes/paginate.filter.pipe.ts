import { Pipe, PipeTransform } from '@angular/core'


@Pipe({
  name : 'paginate',
  pure : false
})

export class PaginatePipe{
  transform(sourceArr : any, page, pagesize  ){
    // console.log(JSON.stringify(sourceArr));
    // console.log('page = ' + page + 'pagesize = ' + pagesize);
    if(sourceArr){
      let startidx : number = ((page-1)*pagesize);
      let endidx : number = startidx + parseInt(pagesize);
      // console.log('computed start index = ' + startidx + 'computed end index = ' + endidx);
      // console.dir(sourceArr);
// console.log('source arr is : ');
// console.dir(sourceArr);
      return sourceArr.slice(startidx,endidx);
    }else{
      console.log('sourceArr is null');
    }
  }
}
