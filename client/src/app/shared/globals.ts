import { Injectable } from  '@angular/core'
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import {Observer} from 'rxjs/Observer';
import { Person } from '../persons/shared/models/person.model'

@Injectable()
export class MyGlobals {
  private isActivePersonForm = false;
  observable_PersonFormChange : Observable<boolean>;
  private _observer: Observer<boolean>;
  private _personToEdit : Person = {id : 0, firstname : ''};
  observable_personToEdit : Observable<any>;
  private _observer_personToEdit: Observer<any>;

  constructor (){
    this.observable_PersonFormChange = new Observable<boolean>(observer => this._observer = observer).share();
    this.observable_personToEdit = new Observable<any>(p_obs => this._observer_personToEdit = p_obs).share();
  }
  setPersonForm( b : boolean){
    this.isActivePersonForm =  b;
    this._observer.next(b);
  }
  getPersonForm(){
    return this.isActivePersonForm;
  }

  setPersonToEdit(p_person : Person){
console.log('globals.ts setPersonToEdit( p_person = ...');
console.dir(p_person);
    this._personToEdit = p_person;
    this._observer_personToEdit.next(p_person);
  }
  getPersonToEdit(){
    return this._personToEdit;
  }


}
