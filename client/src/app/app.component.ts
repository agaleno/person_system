import { Component, Input, OnInit } from '@angular/core';
import { AppHeader} from './shared/viewport/appheader/appheader.component';
import { AppFooter} from './shared/viewport/appfooter/appfooter.component';
import { AppNav} from './shared/viewport/appnav/appnav.component';
import { Routes, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router'
import { PersonsComponent } from './persons/persons.component'
import { PersonFormComponent } from './persons/person-form.component';
import { DocumentationsComponent } from './documentations/documentations.component'
import { PersonsService } from './persons/shared/services/persons.service';
import { MyGlobals } from './shared/globals'

@Component({
	moduleId : module.id,
	selector : 'my-app',
	templateUrl : './app.component.html',
	directives : [AppHeader,AppFooter,AppNav, PersonFormComponent, ROUTER_DIRECTIVES],
	providers : [PersonsService]
})

@Routes([
	{path: '/persons', component : PersonsComponent},
	{path: '/docs', component : DocumentationsComponent}
])

export class AppComponent implements OnInit {
	showPersonForm : boolean;
	globalsSubscription : any;

	// @Input() showPersonForm : boolean = true;
	constructor(private router : Router, private _myglobals : MyGlobals){
  }

  ngOnInit(){
    this.router.navigate(['/persons']);

		this.showPersonForm = this._myglobals.getPersonForm();
		this.globalsSubscription = this._myglobals.observable_PersonFormChange.subscribe( b => this.showPersonForm = b)
		//  console.log('AppComponent	this.mystr  derviced my MyGlobals.redumstr()' + 	this.mystr );
		// this._myglobals.showPersonForm = false;
		// console.log('globals . showPersonForm = ' + this._myglobals.showPersonForm);
  }
}
