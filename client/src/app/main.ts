import { provide } from '@angular/core'
import { bootstrap } from '@angular/platform-browser-dynamic';
import { ROUTER_PROVIDERS} from '@angular/router'
import { AppComponent } from './app.component';
import { HTTP_PROVIDERS} from '@angular/http'
import { MyGlobals } from './shared/globals'



bootstrap(AppComponent,[ROUTER_PROVIDERS, HTTP_PROVIDERS, MyGlobals]);
