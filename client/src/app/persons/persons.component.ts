import { Component, OnInit }  from '@angular/core'
import { Person } from './shared/models/person.model'
import { PersonsService } from './shared/services/persons.service'
import { PersonFormComponent } from './person-form.component'
import { ContainsStringPipe } from '../shared/custompipes/containsstring.filter.pipe'
import { PaginatePipe } from '../shared/custompipes/paginate.filter.pipe'
import { SortByPipe } from '../shared/custompipes/sortby.pipe'
import { PersonContactsComponent } from './contacts.component'
import { PersonEmailsComponent } from './emails.component'
import { MyGlobals } from '../shared/globals'

@Component({
  moduleId : module.id,
  selector : 'app-body',
  templateUrl : 'persons.component.html',
  directives : [PersonFormComponent, PersonContactsComponent, PersonEmailsComponent],
  pipes : [ContainsStringPipe, PaginatePipe, SortByPipe]
})


export class PersonsComponent implements OnInit{
  persons : Person[];
  selectedPerson : Person;
  _personSubscription : any;
  showPersonForm : boolean;
  currentPageNum : number = 1;
  currentPageSize : number = 10;
  searchString : string = '';

  constructor(private _personsService : PersonsService, private _myGlobals : MyGlobals){
  }

  ngOnInit(){
    this.persons = this._personsService.getPersons();
    this._personSubscription = this._personsService._personsObservable.subscribe( p_persons => this.updatePersons(p_persons))
    this.currentPageNum=1;
  }

  showAddPersonModal(){

    this.searchString = '';
    this._myGlobals.setPersonToEdit({id:0, firstname : ''});
    this._myGlobals.setPersonForm(true);
  }

  showEditPersonModal(p_person){
    let personObj : Person = JSON.parse(JSON.stringify(p_person));
    this._myGlobals.setPersonToEdit(personObj);
    this._myGlobals.setPersonForm(true);
  }

  onSelect(p_person){
    this.selectedPerson = p_person;
  }

  updatePersons(p_persons){
    let updatedPersons = new Array<Person>();
    updatedPersons = p_persons;
    this.persons = updatedPersons;
//    this.currentPageNum = 1;
  }


  deletePerson(personId){
    this._personsService.deletePerson(personId)
  }

  goToPreviousPersonPage(){
    if(this.currentPageNum > 1){
      this.currentPageNum--;
    }else{
      this.currentPageNum = 1;
    }
  }

  goToNextPersonPage(){
    if(this.currentPageNum < this.persons.length/this.currentPageSize  ){
      this.currentPageNum++;
    }
  }

  filterPersons(keyword){
    this._personsService.searchPersons(keyword);
  }

  contactarrtostring(arr){
    return arr.map( (x) => { return x.contact} ).toString()
  }

  emailarrtostring(arr){
    return arr.map( (x) => { return x.email} ).toString()
  }


}
