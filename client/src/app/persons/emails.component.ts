import { Component, Input } from '@angular/core'
import { Person } from './shared/models/person.model'
import { PersonsService } from './shared/services/persons.service'

@Component({
  moduleId : module.id,
  selector : 'person-emails',
  templateUrl : './emails.component.html'
})

export class PersonEmailsComponent{
  @Input() person : Person;

  constructor (private _personsService : PersonsService){

  }

  addEmail(personId, newemail){
    this._personsService.addEmail( personId, newemail.value );
    newemail.value = '';
  }

  deleteEmail(pId,eId){
    this._personsService.deleteEmail(pId,eId);
console.log('udner contacs.components; personId = '+ pId + 'emailId : ' + eId);
  }


}
