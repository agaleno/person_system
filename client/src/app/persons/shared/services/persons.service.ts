import { Injectable, OnInit } from '@angular/core'
import { Person } from '../models/person.model'
import { Http, Response } from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { Observer } from 'rxjs/Observer'
import { Observable } from 'rxjs/Observable'
import { MyGlobals } from '../../../shared/globals'

@Injectable()
export class PersonsService implements OnInit {
    persons : Person[];
    _personsObservable  : Observable<Person[]>;
    _personsObserver : Observer<Person[]>;

    constructor(private  _http: Http, private _myGlobals : MyGlobals){

      this._personsObservable = new Observable<Person[]>(observer => this._personsObserver = observer).share();

       if(localStorage.getItem('persons')==null){
          this.persons = []; // to prevent errors
       }else{
           this.persons = JSON.parse(localStorage.getItem('persons'));
      }
    }

    ngOnInit(){

    }
    getPersons(){
      return this.persons;
    }

    searchPersons(keyword){
      if(keyword){
        keyword = keyword.toString().toLowerCase();
        let filteredPersons : Person[] = [];
        let fullnametosearch : string;
        for(let key in this.persons){
          fullnametosearch = this.persons[key]['firstname']+ this.persons[key]['middlename'] + this.persons[key]['lastname'];
          fullnametosearch = fullnametosearch.toLocaleLowerCase();
          if(fullnametosearch.indexOf(keyword) != -1 ){
            filteredPersons.push(this.persons[key]);
          }
        }
        this._personsObserver.next(filteredPersons) ;
      }else{
        this._personsObserver.next(this.persons) ;
      }
    }

    addPerson(newPerson : any){
console.log('person servie addPerson() , newPerson = ...');
console.dir(newPerson);
        newPerson.contacts = [];
        newPerson.emails = [];

        newPerson.creationdate = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();
        newPerson.id=this.persons.length+1;
        this.persons.push(newPerson);
        this._personsObserver.next(this.persons) ;
        localStorage.setItem('persons',JSON.stringify(this.persons));
        this._myGlobals.setPersonToEdit({id : 0, 'firstname' : ''});
    }

    updatePerson(p_personE){
// console.dir(p_personE);
      for( let key in this.persons){
        if(this.persons[key]['id'] == p_personE.id){
          this.persons[key]['firstname']  = p_personE.firstname;
          this.persons[key]['middlename'] = p_personE.middlename;
          this.persons[key]['lastname']   = p_personE.lastname;
          this.persons[key]['city']       = p_personE.city;
          this.persons[key]['country']    = p_personE.country;
          // this.persons[key]['contacts']   = p_personE.contacs;     // will be done in a separate component
          // this.persons[key]['emails']     = p_personE.emails;         // will be done in a separate component
        }
      }
      localStorage.setItem('persons',JSON.stringify(this.persons));
      this._personsObserver.next(this.persons) ;
      this._myGlobals.setPersonToEdit({id : 0, 'firstname' : ''});
    }

    deletePerson(personId){
      for( let key in this.persons){
        if(this.persons[key]['id'] == personId){
          this.persons.splice(+key,1)
        }
      }
      localStorage.setItem('persons',JSON.stringify(this.persons));
    }

    addContacts(personId : number, newContact : string){
// console.log('addContacts  newContact = ...'  + newContact);
      if(newContact.length > 0){
// console.log('ewContact.length  = ' + newContact.length)
        for( let key in this.persons){
          if(this.persons[key]['id'] == personId){
// console.log('person id this.persons[key][id] '+ this.persons[key]['id'] + 'matches personId '+ personId)
            let contactCount = this.persons[key]['contacts'].length;
            this.persons[key]['contacts'].push({ id : contactCount+1, contact : newContact});
          }
        }
        localStorage.setItem('persons',JSON.stringify(this.persons));
      }
    }

    deleteContact(personId, contactId){
        for( let pkey in this.persons){
          if(this.persons[pkey]['id'] == personId){
            for( let ckey in this.persons[pkey]['contacts']){
              if(this.persons[pkey]['contacts'][ckey]['id'] == contactId){
                  this.persons[pkey]['contacts'].splice(ckey,1)
              }
            }
          }
        }
        localStorage.setItem('persons',JSON.stringify(this.persons));
    }

    addEmail(personId, newemailaddr){
      if(newemailaddr.length > 0){
        for( let key in this.persons){
          if(this.persons[key]['id'] == personId){
            let emailcount = this.persons[key]['emails'].length
            this.persons[key]['emails'].push({id : emailcount+1, email: newemailaddr});
          }
        }
        localStorage.setItem('persons',JSON.stringify(this.persons));
      }
    }

    deleteEmail(personId, emailId){
console.log('deleteemail ; personId = ' + personId + ', emailId='+emailId );
        for( let pkey in this.persons){
          if(this.persons[pkey]['id'] == personId){
            for( let ckey in this.persons[pkey]['emails']){
              if(this.persons[pkey]['emails'][ckey]['id'] == emailId){
                console.log(' ____ckey = ' + ckey);
                  this.persons[pkey]['emails'].splice(ckey,1)
              }
            }
          }
        }
        localStorage.setItem('persons',JSON.stringify(this.persons));
    }

  }
