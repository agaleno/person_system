export class Person {

  constructor(
      id : number,
      firstname : string,
      middlename : string = '',
      lastname : string  = '',
      city : string  = '',
      country : string = '',
      emails : string[] = [],
      contacts : string[] = [],
      creationdate: string = ''
  ){

  }

}
