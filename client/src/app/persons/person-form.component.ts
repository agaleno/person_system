import { Component, OnInit } from '@angular/core'
import { Person } from './shared/models/person.model'
import { PersonsService } from './shared/services/persons.service'
import { Validators, ControlGroup, FormBuilder } from '@angular/common'
import { MyGlobals } from '../shared/globals'



@Component({
  moduleId : module.id,
  selector : 'person-form',
  templateUrl : 'person-form.component.html'
  // directives : [PersonService]
})


export class PersonFormComponent implements OnInit {
  newPerson : Person;
  personForm : ControlGroup;
  personToEditSubsription : any;
  personToEdit : Person;
  constructor(  private _personService : PersonsService,
                private _formBuilder : FormBuilder,
                private _myglobals : MyGlobals
              ){
  }

  ngOnInit():any{
    this.personForm = this._formBuilder.group({
      'id'          : [0    ,Validators.required] ,
      'firstname'   : [''   ,Validators.required] ,
      'middlename'  : [''                       ] ,
      'lastname'    : [''   ,Validators.required] ,
      'city'        : [''                       ] ,
      'country'     : [''                       ]
    });
    this.personToEdit = this._myglobals.getPersonToEdit;
    this.personToEditSubsription = this._myglobals.observable_personToEdit.subscribe( p_person => this.personToEdit = p_person)
  }

  onSubmit(p_formPerson, personFormReset){

    if(p_formPerson.id > 0){
      this._personService.updatePerson(p_formPerson);
    }else{
      p_formPerson.contacts = [];
      p_formPerson.emails = [];
      this._personService.addPerson(p_formPerson);
    }
    this.personToEdit = {id : 0, 'firstname' : ''};
    this._myglobals.setPersonToEdit({id : 0, 'firstname' : ''});
    this.hidePersonForm();
    // personFormReset.click(); // to remove chached values
  }

  hidePersonForm(){
      this._myglobals.setPersonForm(false);
  }


}
