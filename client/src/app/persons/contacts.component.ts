import { Component, Input } from '@angular/core'
import { Person } from './shared/models/person.model'
import { PersonsService } from './shared/services/persons.service'

@Component({
  moduleId : module.id,
  selector : 'person-contacts',
  templateUrl : './contacts.component.html',
})

export class PersonContactsComponent{
  @Input() person : Person;

  constructor(private _personsService : PersonsService){}

  addNewContact(personId: number, newContact ){
    this._personsService.addContacts(personId, newContact.value);
    newContact.value = '';
  }

  deleteContact(pId,cId){
    this._personsService.deleteContact(pId,cId);
console.log('udner contacs.components; personId = '+ pId + 'contactId : ' + cId);
  }

}
